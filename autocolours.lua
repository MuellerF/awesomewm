--Script to automatically output colourschemes to various programs in their individual format.
--Supported programs: Alacritty
--
--Defining Class attributes
Colourscheme = {
	name = "",
	bg = 0,
	fg = 0,
	black = "#191919",
	red = 0,
	green = 0,
	yellow = 0,
	blue = 0,
	magenta = 0,
	cyan = 0,
	white = "#acadb1",
	bright = {
		bg = 0,
		fg = 0,
		black = "#36393d",
		red = 0,
		green = 0,
		yellow = 0,
		blue = 0,
		magenta = 0,
		cyan = 0,
		white = "#EEFFFF"
	},
	dim = {
	 	bg = 0,
		fg = 0,
		black = "#676f78",
		red = 0,
		green = 0,
		yellow = 0,
		blue = 0,
		magenta = 0,
		cyan = 0,
		white = "#e0e3e7"
	}
}

--Defining class methods

function Colourscheme:print_attr()
	-- file = io.open('test.yml')
	-- file:write(self.name)
	-- file:close()
	-- -- print(self.bg)
	-- print(self.bright.red)
	-- print(self.black)
	return(self.bg)
end

function Colourscheme:print_alacritty()
	local file =io.open('../../../alacritty/.config/alacritty/alacritty-colours.yml', 'w+')
	file:write("colors:", "\n  primary:", "\n    background: '", self.bg , "'\n    foreground: '", self.fg , "'\n")
	file:write("  cursor:", "\n    text: '", self.bright.bg , "'\n    cursor: '", self.bright.fg , "'\n")
	file:write("  vi_mode_cursor:", "\n    text: '", self.bright.fg , "'\n    cursor: '", self.bright.bg, "'\n" )
	file:write("  selection:", "\n    text: '", self.dim.fg , "'\n    cursor: '", self.dim.bg, "'\n" )
	file:write("  normal:", "\n    black: '", self.black ,
	"'\n    red: '", self.red,
	"'\n    green: '", self.green ,
	"'\n    yellow: '", self.yellow ,
	"'\n    blue: '", self.blue ,
	"'\n    magenta: '", self.magenta ,
	"'\n    cyan: '", self.cyan ,
	"'\n    white: '", self.white , "'\n" )
	file:write("  bright:", "\n    black: '", self.bright.bg,
	"'\n    red: '", self.bright.red,
	"'\n    green: '", self.bright.green,
	"'\n    yellow: '", self.bright.yellow ,
	"'\n    blue: '", self.bright.blue ,
	"'\n    magenta: '", self.bright.magenta ,
	"'\n    cyan: '", self.bright.cyan ,
	"'\n    white: '", self.bright.fg , "'\n" )
	file:write("  dim:", "\n    black: '", self.dim.bg,
	"'\n    red: '", self.dim.red,
	"'\n    green: '", self.dim.green,
	"'\n    yellow: '", self.dim.yellow ,
	"'\n    blue: '", self.dim.blue ,
	"'\n    magenta: '", self.dim.magenta ,
	"'\n    cyan: '", self.dim.cyan ,
	"'\n    white: '", self.dim.fg , "'\n" )
	file:close()
end

--Define the class constructor

function Colourscheme:new(t)
	t = t or {}
	setmetatable(t, self)
	self.__index = self
	return t
end

--Create Object "Material"

material = Colourscheme:new({
	name = "material",
	bg = "#263238" ,
	fg = "#B0BEC5" ,
	red = "#F07178",
	green = "#009688",
	yellow = "#FFCC00",
	blue = "#82AAFF",
	magenta = "#C792EA",
	cyan = "#89DDFF",
	bright = {
		bg = "#192227",
		fg =  "#415967",
		red = "#FF5370",
		green = "#20482C",
		yellow = "#FFCB6B",
		blue = "#717CB4",
		magenta = "#C792EA",
		cyan = "#54d9ff",
	},
	dim = {
	 	bg = "#314549",
		fg = "#EEFFFF",
		red = "#FF9CAC",
		green = "#C3E88D",
		yellow = "#FFCB6B",
		blue = "#717CB4",
		magenta = "#F78C6C",
		cyan = "#8adaf1",
	}
})

-- print ("Object "..material.name.." was created.")

--Call object methods

material:print_attr()
-- material:print_alacritty()
