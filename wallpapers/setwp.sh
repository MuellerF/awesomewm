#!/usr/bin/env bash

#Script to set Wallpapers

#location is .config/wallpapers

# Original script uses the command's argument for location, while the newer version creates the "dir" variable. Uncomment the following line to revert

#[ -d "$1" ] && cp "$(find "$1" -name "*.jpg" -o -name "*.jpeg" -o -name "*.png" -type f | shuf -n 1)" ~/.config/wallpapers/wp.jpg



dir=/home/fm/.config/awesome/wallpapers

cp "$(find $dir -name "*.jpg" -o -name "*.jpeg" -o -name "*.png" -type f | shuf -n 1)" ~/.config/wallpapers/wp

DISPLAY=:0 xwallpaper --center ~/.config/wallpapers/wp
